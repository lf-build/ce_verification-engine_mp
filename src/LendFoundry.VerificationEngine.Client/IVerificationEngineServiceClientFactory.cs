﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.VerificationEngine.Client
{
    public interface IVerificationEngineServiceClientFactory
    {
        IVerificationEngineService Create(ITokenReader reader);
    }
}
