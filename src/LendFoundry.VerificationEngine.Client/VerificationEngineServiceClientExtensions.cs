﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif


namespace LendFoundry.VerificationEngine.Client
{
    public static class VerificationEngineServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddVerificationEngineService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IVerificationEngineServiceClientFactory>(p => new VerificationEngineServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IVerificationEngineServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddVerificationEngineService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IVerificationEngineServiceClientFactory>(p => new VerificationEngineServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IVerificationEngineServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddVerificationEngineService(this IServiceCollection services)
        {
            services.AddTransient<IVerificationEngineServiceClientFactory>(p => new VerificationEngineServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IVerificationEngineServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
