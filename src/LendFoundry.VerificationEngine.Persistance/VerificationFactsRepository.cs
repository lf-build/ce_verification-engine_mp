﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Foundation.Date;

namespace LendFoundry.VerificationEngine.Persistance
{
    public class VerificationFactsRepository : MongoRepository<IVerificationFacts, VerificationFacts>,
        IVerificationFactsRepository
    {
        static VerificationFactsRepository()
        {
            BsonClassMap.RegisterClassMap<VerificationFacts>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.CurrentStatus).SetSerializer(new EnumSerializer<CurrentStatus>(BsonType.String));
                map.MapProperty(p => p.VerificationStatus).SetSerializer(new EnumSerializer<VerificationStatus>(BsonType.String));
                map.MapProperty(p => p.FactType).SetSerializer(new EnumSerializer<FactType>(BsonType.String));
                var type = typeof(VerificationFacts);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<DocumentDetail>(map =>
            {
                map.AutoMap();
                var type = typeof(DocumentDetail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        public VerificationFactsRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "facts")
        {
            CreateIndexIfNotExists("facts_id",
                Builders<IVerificationFacts>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.FactName));

            CreateIndexIfNotExists("facts_with_tenant_id",
                Builders<IVerificationFacts>.IndexKeys.Ascending(i=>i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.FactName));

            CreateIndexIfNotExists("facts_entityId",
                Builders<IVerificationFacts>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId));

            CreateIndexIfNotExists("facts_entityId_status",
                Builders<IVerificationFacts>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId).Ascending(i => i.CurrentStatus));

            CreateIndexIfNotExists("facts_productId",
                Builders<IVerificationFacts>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.ProductId));

            CreateIndexIfNotExists("facts_entityId_pollingId",
                Builders<IVerificationFacts>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId).Ascending(i => i.PollingId));

            CreateIndexIfNotExists("facts_productId_pollingId",
                Builders<IVerificationFacts>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.ProductId).Ascending(i => i.PollingId));
        }

        public async Task<IVerificationFacts> Get(string entityType, string entityId, string factName)
        {
            return await Query.FirstOrDefaultAsync(fact => fact.EntityType == entityType && fact.EntityId == entityId && fact.FactName == factName);
        }

        public async Task<IVerificationFacts> AddOrUpdate(IVerificationFacts verificationFact)
        {
            var fact = await Get(verificationFact.EntityType, verificationFact.EntityId, verificationFact.FactName);

            if (fact == null)
            {
                Add(verificationFact);
                return verificationFact;
            }

            fact.CurrentStatus = verificationFact.CurrentStatus;
            fact.LastVerifiedBy = verificationFact.LastVerifiedBy;
            fact.LastVerifiedDate = verificationFact.LastVerifiedDate;
            fact.VerificationStatus = verificationFact.VerificationStatus;
            fact.CurrentInitiatedMethod = verificationFact.CurrentInitiatedMethod;
            Update(fact);

            return fact;
        }

        public async Task<List<IVerificationFacts>> Get(string entityType, string entityId, List<string> facts, CurrentStatus status)
        {
            return await Query.Where(fact => fact.EntityType == entityType && fact.EntityId == entityId && facts.Contains(fact.FactName) && fact.CurrentStatus == status).ToListAsync();
        }

        public async Task<List<IVerificationFacts>> GetFactDetails(string entityType, string entityId, string productId)
        {
            if (!string.IsNullOrEmpty(productId))
                return await Query.Where(fact => fact.EntityType == entityType && fact.EntityId == entityId && fact.ProductId == productId).ToListAsync();
            else
                return await Query.Where(fact => fact.EntityType == entityType && fact.EntityId == entityId).ToListAsync();
        }

        public async Task<IVerificationFacts> GetFactDetailsByPollingId(string entityType, string entityId, string productId,string pollingId)
        {
            if (!string.IsNullOrEmpty(productId))
                return await Query.FirstOrDefaultAsync(fact => fact.EntityType == entityType && fact.EntityId == entityId && fact.ProductId == productId && fact.PollingId == pollingId);
            else
                return await Query.FirstOrDefaultAsync(fact => fact.EntityType == entityType && fact.EntityId == entityId && fact.PollingId == pollingId);
        }

        public async Task<List<IVerificationFacts>> Get(string entityType, string entityId, CurrentStatus status)
        {
            return await Query.Where(fact => fact.EntityType == entityType && fact.EntityId == entityId && fact.CurrentStatus == status).ToListAsync();
        }


    }
}
