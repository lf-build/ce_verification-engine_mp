using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Security.Encryption;

namespace LendFoundry.VerificationEngine.Persistance
{
    public class VerificationMethodHistoryRepository : MongoRepository<IVerificationMethod, VerificationMethod>,
        IVerificationMethodHistoryRepository
    {
        #region Constructor

        public VerificationMethodHistoryRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "methods-history")
        {
             if (!BsonClassMap.IsClassMapRegistered(typeof(VerificationSources)))
            {
                BsonClassMap.RegisterClassMap<VerificationSources>(map =>
                {
                    map.AutoMap();
                    var type = typeof(VerificationSources);
                    map.MapMember(m => m.DataExtracted).SetSerializer(new BsonEncryptor<object, object>(encrypterService));
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });

            }

            CreateIndexIfNotExists("method_history_id",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.FactName)                 
                    .Ascending(i => i.MethodName));


            CreateIndexIfNotExists("entityType-Id-factname",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.FactName));

            CreateIndexIfNotExists("entityType-Id",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId));
        }
        #endregion

        #region Public Method
        public async Task<IVerificationMethod> Get(string entityType, string entityId, string factName, string methodName)
        {

            return await Query.FirstOrDefaultAsync(
                fact => fact.EntityType == entityType &&
                        fact.EntityId == entityId &&
                        fact.FactName == factName &&
                        fact.MethodName == methodName);
        }

        public async Task<List<IVerificationMethod>> Get(string entityType, string entityId, string factName)
        {
            return await Query.Where(fact => fact.EntityType == entityType &&
                                             fact.EntityId == entityId &&
                                             fact.FactName == factName).ToListAsync();
        }

       
        public async Task<List<IVerificationMethod>> Get(string entityType, string entityId)
        {
            return await Query.Where(fact => fact.EntityType == entityType && fact.EntityId == entityId).ToListAsync();
        }

        public async Task<List<IVerificationMethod>> GetPendingDocuments(string entityType, string entityId, DocumentStatus status)
        {
            return await Query.Where(method => method.EntityType == entityType && method.EntityId == entityId).ToListAsync();
        }

        #endregion
    }
}