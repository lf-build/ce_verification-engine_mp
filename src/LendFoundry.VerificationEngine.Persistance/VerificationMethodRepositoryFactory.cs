﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif


using LendFoundry.Security.Encryption;

namespace  LendFoundry.VerificationEngine.Persistance
{
    public class VerificationMethodRepositoryFactory : IVerificationMethodRepositoryFactory
    {
#region Constructor
        public VerificationMethodRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }
#endregion

#region Private Properties
        private IServiceProvider Provider { get; }
#endregion

#region Public Methods
        public IVerificationMethodRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var configuration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            var encryptionService = Provider.GetService<IEncryptionService>();
            return new VerificationMethodRepository(tenantService, configuration, encryptionService);
        }
#endregion
    }
}