﻿using System;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Foundation.Date;

namespace LendFoundry.VerificationEngine
{
    public class MethodAttemptResult : IMethodAttemptResult
    {
        public VerificationResult Result { get; set; }
        public TimeBucket DateAttempt { get; set; }
        public object DataExtracted { get; set; }
        public string VerifiedBy { get; set; }
        public VerificationRule RuleExecuted { get; set; }
        public string SourceName { get; set; }
        public string Reason { get; set; }
    }
}