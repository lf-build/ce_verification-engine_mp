﻿using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public class DocumentResponse : IDocumentResponse
    {
        public string FactName { get; set; }
        public string MethodName { get; set; }
        public List<string> DocumentName { get; set; }

        public bool IsRequested { get; set; }
        public bool IsDynamic { get; set; }
    }
}
