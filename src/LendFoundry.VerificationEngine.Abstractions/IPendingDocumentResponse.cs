﻿using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public interface IPendingDocumentResponse
    {
        string FactName { get; set; }
        string MethodName { get; set; }
        List<string> DocumentName { get; set; }
        FactType FactType { get; set; }
    }
}
