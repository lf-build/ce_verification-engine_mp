﻿using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationEngineServiceFactory
    {
        IVerificationEngineServiceInternal Create(ITokenReader reader, ITokenHandler handler, ILogger logger, VerificationConfiguration verificationConfiguration);
    }
}