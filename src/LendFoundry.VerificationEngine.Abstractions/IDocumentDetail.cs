﻿namespace LendFoundry.VerificationEngine
{
    public interface IDocumentDetail
    {
        string Category { get; set; }
        string Name { get; set; }
    }
}