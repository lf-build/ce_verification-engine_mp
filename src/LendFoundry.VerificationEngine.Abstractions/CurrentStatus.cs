namespace LendFoundry.VerificationEngine
{
    public enum CurrentStatus
    {
        NotInitiated = 0,
        Initiated = 1,
        InProgress = 2,
        Completed = 3,
        Errored = 4
    }
}