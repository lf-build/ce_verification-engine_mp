﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Application.Document;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationEngineService
    {

        Task<IVerificationMethod> Verify(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, string sourceName, object request);
        Task<IVerificationFacts> GetStatus(string entityType, string entityId, string workFlowStatusId, string factName);
        Task<List<IVerificationMethod>> GetDetails(string entityType, string entityId, string workFlowStatusId, string factName);
        Task<List<IFact>> GetFactList(string productId, string workFlowStatusId);
        Task<List<IFactResponse>> GetFactVerification(string entityType, string entityId, string productId = null, bool doNotLoadMethods = false);
        Task InitiateManualVerification(string entityType, string entityId, string workFlowStatusId, string factName, string currentInitiateMethod);
        Task<IApplicationDocument> AddDocument(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, byte[] file, string fileName);
        Task<List<IPendingDocumentResponse>> GetPendingDocument(string entityType, string entityId, string workFlowStatusId);
        Task<List<IRequiredDocumentResponse>> GetRequiredDocumet(string productId, string workFlowStatusId, string factName);
        Task<string> GetProductId(string entityType, string entityId);

        Task<List<IDocumentResponse>> GetDocuments(string entityType, string entityId, string workFlowStatusId);
        Task EventRePublish(string entityType, string entityId, string workFlowStatusId);

        Task<List<IDocumentWithApplication>> GetDocumentsByFact(string entityType, string entityId, string workFlowStatusId, string factName);
        Task VerifyDocument(string entityType, string entityId, string workFlowStatusId, string factName, string documentId, string statusCode, List<string> reasons = null);

      
        Task ReSetVerification(string entityType, string entityId, IResetFactRequest resetFacts);
        Task<IVerificationFacts> GetFactDetailsByPollingId(string entityType, string entityId, string productId, string pollingId);
        Task InitiateSyndicationVerification(string entityType, string entityId, string workFlowStatusId, string factName, string currentInitiateMethod);
        Task InitiateDynamicFactVerification(string entityType, string entityId, string workFlowStatusId, IDynamicFactRequest dynamicFactRequest);
    }
}