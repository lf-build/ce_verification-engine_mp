﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationFacts : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string FactName { get; set; }
        CurrentStatus CurrentStatus { get; set; }
        VerificationStatus VerificationStatus { get; set; }
        TimeBucket LastVerifiedDate { get; set; }
        string LastVerifiedBy { get; set; }
         string ProductId { get; set; }
         string StatusWorkFlowId { get; set; }
        string CurrentInitiatedMethod { get; set; }

        string PollingId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDocumentDetail, DocumentDetail>))]
        List<IDocumentDetail> DocumentDetails { get; set; }
        FactType FactType { get; set; }
    }
}
