﻿namespace LendFoundry.VerificationEngine
{
    public class ManualFactVerificationInitiated
    {
        public ManualFactVerificationInitiated()
        {
        }

        public ManualFactVerificationInitiated(string entityType,string entityId,string fact)
        {
            EntityType = entityType;
            EntityId = entityId;
            Fact = fact;
        }
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string Fact { get; set; }
    }
}
