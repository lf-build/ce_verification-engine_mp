﻿using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public class VerificationExecutionResult
    {
        public string Result { get; set; }
        public string Reason { get; set; }
        public object Data { get; set; }
    }
}
