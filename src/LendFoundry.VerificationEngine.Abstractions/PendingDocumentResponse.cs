﻿using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public class PendingDocumentResponse : IPendingDocumentResponse
    {
        public string FactName { get; set; }
        public string MethodName { get; set; }
        public List<string> DocumentName { get; set; }
        public FactType FactType { get; set; }
    }
}
