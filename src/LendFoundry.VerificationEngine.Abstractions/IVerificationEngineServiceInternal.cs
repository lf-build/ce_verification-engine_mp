﻿using System.Threading.Tasks;
using LendFoundry.Application.Document;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationEngineServiceInternal : IVerificationEngineService
    {
        Task<IVerificationMethod> Verify(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, string sourceName, bool isManual, string productId,  object input);
        Task InitiateFactBasedOnChangeInCategory(string oldCategory, string workFlowStatusId, IApplicationDocument document);
    }
}