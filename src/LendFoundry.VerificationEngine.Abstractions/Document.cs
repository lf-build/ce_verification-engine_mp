﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public class Document : IDocument
    {
      public  string DocumentGeneratedId { get; set; }

        public DocumentStatus DocumentStatus { get; set; }

        public List<string> Reasons { get; set; }
      
    }
}
