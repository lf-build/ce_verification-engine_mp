﻿using System;


namespace LendFoundry.VerificationEngine
{
    public class VerificationMethodInitiationFailed : Exception
    {
        public VerificationMethodInitiationFailed(string message) : base(message)
        {

        }
    }
}
