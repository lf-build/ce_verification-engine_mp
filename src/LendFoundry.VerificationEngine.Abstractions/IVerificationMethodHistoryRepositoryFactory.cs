﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationMethodHistoryRepositoryFactory
    {
        IVerificationMethodHistoryRepository Create(ITokenReader reader);
    }
}