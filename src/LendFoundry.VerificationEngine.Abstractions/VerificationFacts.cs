﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public class VerificationFacts : Aggregate, IVerificationFacts
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string FactName { get; set; }
        public CurrentStatus CurrentStatus { get; set; }
        public VerificationStatus VerificationStatus { get; set; }
        public TimeBucket LastVerifiedDate { get; set; }
        public string LastVerifiedBy { get; set; }

        public string CurrentInitiatedMethod { get; set; }
        public string ProductId { get; set; }

        public string StatusWorkFlowId { get; set; }

        public string PollingId { get; set; }

        [BsonIgnore]
        public string MethodName { get; set; }

        [BsonIgnore]
        public string RequestType { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDocumentDetail, DocumentDetail>))]
        public List<IDocumentDetail> DocumentDetails { get; set; }
        public FactType FactType { get; set; }
    }
}
