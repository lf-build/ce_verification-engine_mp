﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public interface IResetFactRequest
    {
        List<string> FactNames { get; set; }
    }
}
