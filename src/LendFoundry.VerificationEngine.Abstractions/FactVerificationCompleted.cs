﻿namespace LendFoundry.VerificationEngine
{
    public class FactVerificationCompleted
    {
        public object Fact { get; set; }
        public MethodAttemptResult LatestMethodAttempt { get; set; }
        public string Reason { get; set; }

        public FactVerificationCompleted()
        {

        }

        public FactVerificationCompleted(object fact, MethodAttemptResult latestMethodAttempt)
        {
            Fact = fact;
            LatestMethodAttempt = latestMethodAttempt;
            Reason = null;
            if (latestMethodAttempt!=null && latestMethodAttempt.Reason != null)
            {
                Reason =  latestMethodAttempt.Reason;
            }
        }

    }

}
