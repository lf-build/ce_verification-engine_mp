using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationSources
    {
        string SourceName { get; set; }
        object DataExtracted { get; set; }
        TimeBucket DataExtractedDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        ISource Source { get; set; }
    }
}