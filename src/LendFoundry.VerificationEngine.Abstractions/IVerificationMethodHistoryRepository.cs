﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationMethodHistoryRepository : IRepository<IVerificationMethod>
    {
        Task<IVerificationMethod> Get(string entityType, string entityId, string factName, string methodName);
        Task<List<IVerificationMethod>> Get(string entityType, string entityId, string factName);
       
        Task<List<IVerificationMethod>> Get(string entityType, string entityId);
     
        Task<List<IVerificationMethod>> GetPendingDocuments(string entityType, string entityId, DocumentStatus status);
    }
}