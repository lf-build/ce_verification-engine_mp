﻿namespace LendFoundry.VerificationEngine.Configuration
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string SyndicationName { get; set; }
        public string Response { get; set; }
        public string ReferenceNumber { get; set; }
        public string SourceType { get; set; }
    }
}