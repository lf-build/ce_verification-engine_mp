﻿using LendFoundry.Voting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine.Configuration
{
    public class Polling
    {
        public string PollName { get; set; }

        public string Description { get; set; }
        public List<Option> options { get; set; }
        public List<Participant> participants { get; set; }

        public string CloseOnDays { get; set; }
    }
}
