﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.VerificationEngine.Configuration
{
    public class VerificationConfiguration : IDependencyConfiguration
    {
        public CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<Fact>>> Facts { get; set; }
        public List<EventMapping> Events { get; set; }
        public bool SecondaryKeyEnable { get; set; }
        public string SecondaryRuleName { get; set; }
        public bool UseDecisionEngine { get; set; }
        public bool CanRuleReturnVerificationResult { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
