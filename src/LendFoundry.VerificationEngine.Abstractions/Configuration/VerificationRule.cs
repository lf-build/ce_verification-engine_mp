namespace LendFoundry.VerificationEngine.Configuration
{
    public class VerificationRule
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public bool CanRuleReturnVerificationResult { get; set; }
    }
}