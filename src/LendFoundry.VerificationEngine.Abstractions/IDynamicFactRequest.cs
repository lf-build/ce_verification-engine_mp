﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public interface IDynamicFactRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IDocumentDetail, DocumentDetail>))]
        List<IDocumentDetail> DocumentDetails { get; set; }
        string FactName { get; set; }
    }
}